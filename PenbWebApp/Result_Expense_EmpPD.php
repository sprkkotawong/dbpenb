<!--SEARCH EMPLOYEE-->
<?php 
session_start();
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "DBpenb";

//create connection
$conn = new mysqli($servername,$username,$password,$dbname); 
?>
<?php
function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}
?>
<?php 
if($arr[0]=='A'|| $arr[0]=='a'){
if(isset($_POST['expense-emp-pd-view']) || isset($_POST['expense-emp-pd-submit'])){
    $expense_emp_date = $_POST["expense-emp-date"];
    $sum_Emp_Fair = $conn->query("SELECT * FROM work_on INNER JOIN employee ON work_on.Emp_ID = employee.Emp_ID WHERE '$expense_emp_date' = W_Date");
}
}else{
    header('Location: /Home.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="styleHome.css" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Result Search EMP</title>
</head>

<body>
    <center>
        <div style="margin-bottom:10px;">
            <a href="Home.php">
                <button type="submit" style="height:30px" id="backBtn">
                    Back
                </button>
            </a>
        </div>
        <center>
            <table id="myTable">
                <thead>
                    <tr class="header">
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Day</th>
                        <th>Hours</th>
                        <th>Minute</th>
                        <th>Second</th>
                        <th>Total Hours</th>
                        <th>Earning</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
            $sum_price = 0;
      while($row = $sum_Emp_Fair->fetch_assoc()){
        $Login_Time = new DateTime($row['Emp_Start']);
        $Logout_Time = new DateTime($row['Emp_Stop']);
        $interval = $Login_Time->diff($Logout_Time);
        $hours = ceil($interval->d*24 + $interval->h + $interval->i/60 + $interval->s/3600);
        $price = $hours*50;
        echo "<tr>"; 
        echo "<td>".$row['Emp_ID']."</td>";
        echo "<td>".$row['Fname']."</td>";
        echo "<td>".$row['Lname']."</td>";
        echo "<td>".($interval->d)."</td>"; //day *24 = hr
        echo "<td>".$interval->h."</td>"; //hr 
        echo "<td>".$interval->i."</td>"; //mn /60 = hr
        echo "<td>".$interval->s."</td>"; //sc /60*60 = hr
        echo "<td>" . $hours ." Hrs"."</td>";
        echo "<td>" . $price ." THB"."</td>";
        echo "<tr>"; 
        $sum_price += $price;
       }
       echo "<h1>"."Total Price : ".$sum_price."</h1>";
       if(isset($_POST['expense-emp-pd-submit'])){
        $updateSummary = "UPDATE summary SET Total_Expense = $sum_price WHERE S_Date = '$expense_emp_date' ";
        // if(!($conn->query($updateSummary))){
        //  echo "Error from UpDate : ".$conn->error;
        //  }
       }
            ?>
                </tbody>
            </table>
</body>

</html>